FROM alpine:3.14

# Install necessary packages in the container image.

# This image has openssh-client, 
#   so backups can be sent to a remote server over SSH - using rsync.

RUN   apk update; apk add bzip2 coreutils curl gzip mysql-client openssh-client rsync tar zip 

# The heart of this docker image is this file:
COPY backup-mysql-databases.sh /backup-mysql-databases.sh

ENTRYPOINT  /backup-mysql-databases.sh
 

# The file below is being copied in the container image, 
#   to simply package it in the container.
# It can be extracted from the container image and saved on the host OS,
#   and used to run this container image properly in plain docker environments.
# It is not required in Kubernetes environments.
#  
COPY connect-to-mysql-container.sh /connect-to-mysql-container.sh






############################################################################
# Notes:
# * coreutils is needed for 'date "days ago"' to work correctly on Alpine Linux.
# * mysql-client gives us: mysql, mysqldump, etc
# * openssh-client give us: ssh, ssh-keyscan, etc.
