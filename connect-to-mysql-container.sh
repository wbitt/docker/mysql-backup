#!/bin/sh

# This script performs backup of mysql databases from an existing
#   mysql container.
# It connects to the container, performs the backup, and places the files
#   in a directory, which is mounted from the host OS.
#
# It requires some variables to be passed as env variables.
# * MYSQL_CONTAINER_NAME - (mandatory) (default value: none)
# * HOST_SECRETS_FILE - (optional)
#      (default value: /home/containers-secrets/mysql-backup/mysql-backup.env
# * HOST_BACKUP_DIRECTORY - (optional)
#      (default value: /home/containers-data/mysql-backup/

##################### START -  Set default values ######################
#
# These configuration variables can be passed from host OS environment,
#   using environment variables or a configuration file.
 
# MYSQL_CONTAINER_NAME:
# On the host OS, this will be the name of the MYSQL service container,
#   to which we need to connect and perform database backup using mysqldump.
MYSQL_CONTAINER_NAME=${MYSQL_CONTAINER_NAME:-mysql_mysql.local_1}

# HOST_SECRETS_FILE:
# On the host OS, this will be a file, which contains necessary 
#   environment variables required for "mysqldump" to connect to
#   the target mysql service container, and perform backup.
HOST_SECRETS_FILE=${HOST_SECRETS_FILE:-/home/containers-secrets/mysql-backup/mysql-backup.env}

# HOST_BACKUP_DIRECTORY:
# ---------------------
# On the host OS, it will be a directory which the admin/user will mount 
#   inside the container.
# HOST_BACKUP_DIRECTORY
HOST_BACKUP_DIRECTORY=${HOST_BACKUP_DIRECTORY:-/home/containers-data/mysql-backup/}

################ END -  Configuration required by the main script ###### 


# Environment variables required by the mysql client container, 
#   loaded from an ENV/secrets file "HOST_SECRETS_FILE"

# -------------------START - Contents of secrets file ------------------
# MYSQL_HOST=127.0.0.1
# MYSQL_ROOT_USER=root
# MYSQL_ROOT_PASSWORD=WHATEVER-ROOT-PASSWORD-IS
# NICE_VALUE=0
#
# ------------------- END - Contents of secrets file -------------------



########################### START - System variables ###################
#

DOCKER=$(which docker)

# This script is part of the container image.
#   It is supposed to be on this path. 
# This script is run as DOCKER_ENTRYPOINT in the client script.
MYSQL_BACKUP_SCRIPT=/backup-mysql-databases.sh

## TODAY=$(date +%Y%m%d)
TODAY=$(date +%Y%m%d)

TIMESTAMP=$(date +%Y%m%d-%H%M)

# Main directory where today's backups will be stored
TODAY_BACKUPS_DIRECTORY=${TODAY}

#
########################### END - System variables #####################



############################ START - Sanity checks #####################
#




if [ -z "${MYSQL_CONTAINER_NAME}" ]; then
  echo "MYSQL_CONTAINER_NAME is empty."
  echo "You must provide the name of the MySQL DB container."
  echo "Use: 'docker ps' to find the name of the MySQL DB container"
  echo "Then, re-run the program as:"
  echo
  echo "source ./connect-to-mysql-container.env ; ./connect-to-mysql-container.sh"
  echo
  exit 1
else 
  echo "MYSQL_CONTAINER_NAME is - ${MYSQL_CONTAINER_NAME}"
fi

if [ -z "${DOCKER}" ]; then
  echo "docker executable not found. Exiting ..."
  exit 1
else
  echo "docker executable is found."
  docker --version
fi


echo

CHECK_MYSQL_CONTAINER=$(${DOCKER} container ls --filter "status=running" --filter "name=${MYSQL_CONTAINER_NAME}" --format "{{.Names}}")

if [ -z "${CHECK_MYSQL_CONTAINER}" ]; then
  echo "The MYSQL service container name - ${MYSQL_CONTAINER_NAME} - was not found. Exiting ..."
  exit 1
fi


# This directory path is from the host OS.
if [ ! -d ${HOST_BACKUP_DIRECTORY}  ]; then
  echo "HOST_BACKUP_DIRECTORY - ${HOST_BACKUP_DIRECTORY} does not exist. Exiting ..."
  exit 1
else
  echo "HOST_BACKUP_DIRECTORY is found as - ${HOST_BACKUP_DIRECTORY} . (This is where you will find your backups on the host system.)" 

fi


if [ ! -r ${HOST_SECRETS_FILE}  ]; then
  echo "HOST_SECRETS_FILE - ${HOST_SECRETS_FILE} is not readable. Exiting ..."
  exit 1
else
  echo "HOST_SECRETS_FILE being used is - ${HOST_SECRETS_FILE}"

fi


#
############################ END - Sanity checks #######################

echo
echo "Executing docker command -"
echo "docker run --rm --env-file=${HOST_SECRETS_FILE} \ "
echo "           --network container:${MYSQL_CONTAINER_NAME} \ "
echo "           --volume ${HOST_BACKUP_DIRECTORY}:/backups \ "
echo "           --pull=always kamranazeem/mysql-backup"
echo

echo "---> All output below will be from the container performing the actual backup. <---"
echo

docker run --rm --env-file=${HOST_SECRETS_FILE} \
           --network container:${MYSQL_CONTAINER_NAME} \
           --volume ${HOST_BACKUP_DIRECTORY}:/backups \
           --pull=always kamranazeem/mysql-backup

echo
echo "Docker run complete."
echo
echo "Back on the host system - the backups are located in - ${HOST_BACKUP_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY} . Below is list of files from this directory."
echo 
ls -lha  ${HOST_BACKUP_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY}

echo
