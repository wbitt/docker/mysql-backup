# Backup MySQL databases from inside the containers:
This container image helps backup MySQL databases from a MySQL database container, and optionally transport them to a remote backup location using SSH/RSYNC, AWS S3, GCP, Digitial Ocean, etc.

The heart of this docker image is the `backup-mysql-databases.sh` script. 

The script takes several environment variables, which help it to connect to a MySQL database instance , take backup of databases, and also copy the database backup files to a remote location.

------

## How to use this image on docker host:

To perform backup of MySQL databases running inside a MySQL container, follow these steps. 


Start by cloning this repository to a proper location.

Execute as `root`:

```
cd /opt

git clone https://gitlab.com/wbitt/docker/mysql-backup.git

chown -R deployer:deployer mysql-backup    # optional

su - deployer 

cd /opt/mysql-backup
```

Copy the `connect-to-mysql-container.env.example` file as `connect-to-mysql-container.env` , and adjust the target file.

```
$ cp connect-to-mysql-container.env.example   connect-to-mysql-container.env
```

```
$ vi connect-to-mysql-container.env

export MYSQL_CONTAINER_NAME=mysql_mysql.local_1
export HOST_SECRETS_FILE=/home/containers-secrets/mysql-backup/mysql-backup.env
export HOST_BACKUP_DIRECTORY=/home/containers-data/mysql-backup/
```

To fill in the name of `MYSQL_CONTAINER_NAME` in the file above, you can use the following command:

```
$ docker ps
CONTAINER ID   IMAGE              COMMAND                  CREATED        STATUS        PORTS                                      NAMES
2397f0dedd38   wordpress:latest   "docker-entrypoint.s…"   18 hours ago   Up 18 hours   80/tcp                                     mrwtraderscom_mrwtraders.com_1
981535774e73   mysql:5.7          "docker-entrypoint.s…"   18 hours ago   Up 5 hours    0.0.0.0:3306->3306/tcp, 33060/tcp          mysql_mysql.local_1
4f9b3cc92cc5   traefik:1.7        "/traefik"               18 hours ago   Up 18 hours   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   00-traefik-proxy_traefik_1
```

From the output above, we can see it to: "mysql_mysql.local_1"


Now, copy the `mysql-backup.env.example` file as `/home/containers-secrets/mysql-backup/mysql-backup.env` , and adjust the target file.

```
$ cp mysql-backup.env.example /home/containers-secrets/mysql-backup/mysql-backup.env
```


The contents of `mysql-backup.env` look like this:
```
$ vi /home/containers-secrets/mysql-backup/mysql-backup.env

MYSQL_ROOT_PASSWORD=your-mysql-root-password

MYSQL_HOST=127.0.0.1
MYSQL_ROOT_USER=root
NICE_VALUE=19
REMOTE_BACKUP_METHOD=none
IGNORE_DATABASES=
BACKUP_DATABASES=
RETENTION_PERIOD=14
BACKUPS_DIRECTORY=/backups
HOSTNAME=db-server.example.com
```

Now, pull the latest version of the docker image:

```
docker pull kamranazeem/mysql-backup
```

Now, run it like this:

```
source ./connect-to-mysql-container.env ; ./connect-to-mysql-container.sh
```


Now, setup a cron job to run this every day at 1 AM, as user `root`.

```
[root@server2 ~]# crontab -e
. . . 

0 1 * * * source /opt/mysql-backup/connect-to-mysql-container.env ; /opt/mysql-backup/connect-to-mysql-container.sh

[root@server2 ~]# 
```


## Run the container image directly on docker host without the external script:

Run the mysql-backup container like this to perform the backup from a MySQL db container:

```
MYSQL_CONTAINER_NAME=mysql_mysql.local_1
HOST_SECRETS_FILE=/home/containers-secrets/mysql-backup/mysql-backup.env
HOST_BACKUP_DIRECTORY=/home/containers-data/mysql-backup/

docker run  --env-file=${HOST_SECRETS_FILE} \
            --network container:${MYSQL_CONTAINER_NAME} \
            --volume ${HOST_BACKUP_DIRECTORY}:/backups \
            --pull=always kamranazeem/mysql-backup

```


If you want to simply look around inside the container, or to troubleshoot, use `--entrypoint=""`:
```
docker run  -it --entrypoint="" \
            --env-file=${HOST_SECRETS_FILE} \
            --network container:${MYSQL_CONTAINER_NAME} \
            --volume ${HOST_BACKUP_DIRECTORY}:/backups \
            --pull=always kamranazeem/mysql-backup  /bin/sh

```

Or, even more simpler version:
```
docker run  --rm --it --entrypoint="" kamranazeem/mysql-backup /bin/sh
```

------

