#!/bin/sh

########################################################################
# Author:   Muhammad Kamran Azeem (kamran@wbitt.com)
# Created:  2008-04-24
# Modified: 20210721, 20130803, 20120120, 20080720, 20080628
#
# Notes:    * For FTP, make sure that the file ~/.netrc exists in the home directory
#               of the user running this script. The permissions must be 0600.
#               The syntax of .netrc file is:
#               ```
#               machine ftpserver.somedomain.com
#               login ftpusername
#               password somepassword
#               ```
#           * For MySQL login without root password, 
#               you can also create a '.my.cnf' file in the user's home directory.
#               Normally inside '/root/' as most of the time it is 'root' executing cron jobs.
#               The syntax of '.my.cnf' file is:
#               ```
#               [client]
#               user=user
#               password=password
#               ```
#
#           * This script must have permissions not more than 0700, and owned by root . Because
#               this script contains username and password of your mysql admin user.
#
#           * You will need to frequently delete the old backups both from local disk,
#               as well as from the remote server(s).
#
#           * Setup a cronjob as:- (run everyday at 3 AM)
#               0 3 * * * /path/backup-mysql-databases.sh
#
########################################################################


########## START - User configuration ##################################
#

# Notes: Variables required from outside the script, 
#          e.g. passed from environment/secrets file:
#
# * MYSQL_HOST (default - 127.0.0.1)
# * MYSQL_ROOT_USER  (default - root)
# * MYSQL_ROOT_PASSWORD (default - none) [mandatory]
# * NICE_VALUE (0 to 19) (default - 19) 
#     (0 = backup process get more system resources - fast, but high load on DB server)
#     (19 = backup process gets less system resources - slow, but less load on DB server)
# * REMOTE_BACKUP_METHOD (default - none) (SSH, S3, DO, GCP)
#     Each method requires it's own set of variables to work correctly.
# * IGNORE_DATABASES  (default - null) (list of databases to ignore, separated by space)
# * BACKUP_DATABASES (default - null) (list of databases to (only) take backup for)
# * RETENTION_PERIOD (default - 14) (number of days to keep backup for)
# * BACKUPS_DIRECTORY (default - /backups) (location where this script will keep db backup files)
#                     (When run inside container, /backups is a safe choice)
# * HOSTNAME (default - value of `hostname -f`) (useful to set manually,
#                     when the backup script is running inside a container,
#                     and the dump filenames contain the name of container,
#                     instead of the hostname of the docker host)

# NOTE: Without MYSQL_ROOT_PASSWORD variable this script will not work.



# IGNORE_DATABASES:
# DO NOT BACKUP these databases. 
# The list is db names separated by space, enclosed by quotation marks.
# If you want to skip any databases, add them to this list.
# IGNORE_DATABASES="postcodes"

# BACKUP_DATABASES:
# Only backup these databases. Being opposite of IGNORE_DATABASES, 
#   this one takes list of databases to backup.
# Useful when you want to backup a specific database only. 
# When this variable is present and is not null, 
#   then this script will not perform backup of all databases. Instead,
#   it will backup ONLY the ones mentioned.
# BACKUP_DATABASES="foodshop petshop"

# RETENTION_PERIOD:
# This is the time period for which the backups will be retained.
# Backups before this date will be deleted! Make selection carefully.
# The value is number of days before $TODAY.
# Note: This works only if the BACKUP_DESTINATION_DIRECTORY is a persistent storage.
#       If it is not, the script will simply not find old backup,
#          and will simply not delete it.
#
# RETENTION_PERIOD=14


# BACKUPS_DIRECTORY:
# If you leave this empty, it will automatically be set to "/backups".
#   Never set it to "/".
# BACKUPS_DIRECTORY="/backups"

#
########## END - User configuration ####################################




################## START - Set default values where required ###########
#

BACKUPS_DIRECTORY=${BACKUPS_DIRECTORY:-/backups}
RETENTION_PERIOD=${RETENTION_PERIOD:-14}

ALWAYS_IGNORE_DB_LIST="information_schema performance_schema test"
IGNORE_DB_LIST="${ALWAYS_IGNORE_DB_LIST} ${IGNORE_DATABASES}"

# Set NICE_VALUE to 0 (default) if nothing is mentioned. 
# To have less effect on the DB server/service, you can choose to use 19.
# To do that, pass the NICE_VALUE variable set to any number between 0 to 19.
# Note: Below the number is (positive) '19', not '-19'. The ':-' is part of the syntax.
NICE_VALUE=${NICE_VALUE:-19}

REMOTE_BACKUP_METHOD=${REMOTE_BACKUP_METHOD:-none}

HOSTNAME=${HOSTNAME:-$(hostname -f)}

#
################## END - Set default values where required ###########




################### START - Sanity checking ############################



if [ -z "${BACKUPS_DIRECTORY}" ] || [ "${BACKUPS_DIRECTORY}" == "/" ] ; then

  echo "BACKUPS_DIRECTORY cannot be empty, nor it can be equal to / ."
  echo "If you do so, you can potentially delete all data on your disks."
  echo "Fix this, and re-run the program."
  exit 1
else
  echo "BACKUPS_DIRECTORY is - ${BACKUPS_DIRECTORY}"
fi


if [[ -z "${MYSQL_HOST}" ]] ; then
  MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
  echo "Variable MYSQL_HOST is empty. Setting it to '127.0.0.1'"
else
  echo "Variable MYSQL_HOST is - ${MYSQL_HOST}"
fi 


if [[ -z "${MYSQL_ROOT_USER}" ]] ; then
  MYSQL_ROOT_USER=${MYSQL_ROOT_USER:-root}
  echo "Variable MYSQL_ROOT_USER is empty. Setting it to 'root'"
else
  echo "Variable MYSQL_ROOT_USER is - ${MYSQL_ROOT_USER}"
fi 

if [[ -z "${MYSQL_ROOT_PASSWORD}" ]] ; then
  echo "Variable MYSQL_ROOT_PASSWORD is empty. Exiting ..."
  exit 1
else
  echo "Variable MYSQL_ROOT_PASSWORD is - [REDACTED]"
fi 


#
##################### END - Sanity checking ############################



##################### START - Main Program #############################
#


MYSQLHOST="${MYSQL_HOST}"  # MySQL Hostname or the name of mysql service to connect to
MYSQLUSER="${MYSQL_ROOT_USER}"  # MySQL root USERNAME
MYSQLPASS="${MYSQL_ROOT_PASSWORD}"   # MySQL root PASSWORD

## TODAY=$(date +%Y%m%d)
TODAY=$(date +%Y%m%d)

TIMESTAMP=$(date +%Y%m%d-%H%M)

# Main directory where today's backups will be stored
TODAY_BACKUPS_DIRECTORY=${TODAY}

# Directory to remove after backup is complete.
REMOVE_LAST_BACKUP_DATE=$(date --date="${RETENTION_PERIOD} days ago" +%Y%m%d)
# Note: For any OS, which uses busybox for different OS utilities, 
#         ensure that coreutils package is installed.
# On Alpine Linux, date comes from busybox, which doesn't work correctly
#   unless "coreutils" package exists on the system.

if [ -z "${REMOVE_LAST_BACKUP_DATE}" ]; then
  echo "This OS has the date utility, which behaves differently than expected."
  echo "This is usually the case when date utility is part of busybox,"
  echo "  which, in-turn is the case with Alpine OS, and busybox docker image."
  echo "In such a case, we set REMOVE_LAST_BACKUP_DATE to 19700101,"
  echo "  so the script does not fail unexpectedly."
  REMOVE_LAST_BACKUP_DATE=${REMOVE_LAST_BACKUP_DATE:-19700101}
fi


# Temporary Password file for MySQL:
MYSQL_DEFAULTS_FILE=${HOME}/${TIMESTAMP}.my.cnf

# Paths to various utlities / programs ...
TAR=$(which tar)
MYSQL=$(which mysql)
MYSQLDUMP=$(which mysqldump)
BZIP2=$(which bzip2)
RSYNC=$(which rsync)
NICE=$(which nice)


# File to store current backup file
DUMP_FILE=""

# Store list of databases
DB_LIST=""


echo "Here is the configuration, you specified."
echo ""
echo "MYSQLHOST=${MYSQLHOST}"
echo "MYSQLUSER=${MYSQLUSER}"
echo "MYSQLPASS=[REDACTED]"
echo "BACKUPS_DIRECTORY=${BACKUPS_DIRECTORY}"
echo "TODAY_BACKUPS_DIRECTORY=${TODAY_BACKUPS_DIRECTORY}"
echo "RETENTION_PERIOD=${RETENTION_PERIOD}"
echo "REMOVE_LAST_BACKUP_DATE=${REMOVE_LAST_BACKUP_DATE}"
echo "System HOSTNAME=${HOSTNAME}"
echo "IGNORE_DATABASES=${IGNORE_DATABASES}"
echo "BACKUP_DATABASES=${BACKUP_DATABASES}"
echo "REMOTE_BACKUP_METHOD=${REMOTE_BACKUP_METHOD}"



if [ ! -d ${BACKUPS_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY} ]; then
  mkdir -p ${BACKUPS_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY}
fi


############ START - Create .my.cnf #################


cat > ${MYSQL_DEFAULTS_FILE} <<EOF
[client]
host=${MYSQLHOST}
user=${MYSQLUSER}
password=${MYSQLPASS}
EOF

chmod 0600 ${MYSQL_DEFAULTS_FILE}

############ END - Create .my.cnf #################


# Get all database list first
DB_LIST=$(${MYSQL} --defaults-file=${MYSQL_DEFAULTS_FILE} -Bse 'show databases')

# If BACKUP_DATABASES is present, only backup those databases:
if [ -n  "${BACKUP_DATABASES}" ]; then
  DB_LIST=${BACKUP_DATABASES}
fi 

echo
echo "Starting DB backup/dump for the following databases ...."
echo ${DB_LIST}


echo
for DB in ${DB_LIST}; do

  BACKUP_THIS_DB="TRUE"

  echo "--------------------------------------------------------------------------------"

  if [[ "${IGNORE_DB_LIST}" != "" ]] && [[ "${IGNORE_DB_LIST}" =~ "${DB}" ]] ; then
    echo "***** This DB (${DB}) is set to be ignored in IGNORE_DB_LIST variable. Skipping ..."
    BACKUP_THIS_DB="FALSE"
  fi

  if [ "${BACKUP_THIS_DB}" == "TRUE" ] ; then
    DUMP_FILE="${BACKUPS_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY}/${DB}.${HOSTNAME}.${TIMESTAMP}.dump"
    echo "Performing dump/backup of database - ${DB} in file - ${DUMP_FILE} ..."
    echo "Executing - ${NICE} -n ${NICE_VALUE} ${MYSQLDUMP}  --defaults-file=${MYSQL_DEFAULTS_FILE} --routines --triggers ${DB} > ${DUMP_FILE}"
    ${NICE} -n ${NICE_VALUE} ${MYSQLDUMP} --defaults-file=${MYSQL_DEFAULTS_FILE} \
      --routines --triggers ${DB} > ${DUMP_FILE}
    # https://serverfault.com/questions/912162/mysqldump-throws-unknown-table-column-statistics-in-information-schema-1109
  fi
done

echo ""
echo ""
echo "Database Dump/Backup complete."
echo ""
echo "=================================================================="
echo ""
echo ""

echo

if [ -z "${BZIP2}" ]; then
  echo "bzip2 was not found on the system. DB dump files will not be compressed."

else
  echo "Now compressing database dump files ..."
  echo "Executing - ${NICE} -n ${NICE_VALUE} ${BZIP2} -9 ${BACKUPS_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY}/*.dump"
  ${NICE} -n ${NICE_VALUE} ${BZIP2} -9 ${BACKUPS_DIRECTORY}/${TODAY_BACKUPS_DIRECTORY}/*.dump

fi


if [[ -d ${BACKUPS_DIRECTORY}/${REMOVE_LAST_BACKUP_DATE} ]]; then 
  echo
  echo "Time to remove old backups from local system ..."
  echo "Removing the directory ${BACKUPS_DIRECTORY}/${REMOVE_LAST_BACKUP_DATE} ..."
  echo "Executing - rm -fr ${BACKUPS_DIRECTORY}/${REMOVE_LAST_BACKUP_DATE}*"
  rm -fr ${BACKUPS_DIRECTORY}/${REMOVE_LAST_BACKUP_DATE} \
     && echo "Old backup directory successfully removed."
fi 

rm -f ${MYSQL_DEFAULTS_FILE}

echo ""
echo ""
echo "Program execution complete."
echo ""
echo "=================================================================="
echo ""
echo ""

#
########################## END - Main Program ##########################

